
const restify 	= require('restify')
const settings  = require("./settings");
const sqlFun 	= require('./controllers/sqlfunction')

const server = restify.createServer();

server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())

// routes
server.get('/user', (req, resp) => {
	sqlFun.getlist(req, resp)
});

server.get('/formulario', (req, resp) => {
	sqlFun.getform(req, resp)
});

server.get('/user/:id', (req, resp) => {
	const id = parseInt(req.params.id)
	console.log(id)
	sqlFun.get(req, resp, id)
});

server.listen(3000, () => {
	//console.log (settings.app.port)
	console.log('Escuchando en http://localhost:'+ settings.app.port);
})


/*

const users = 
{
	1: { name: 'John',lastName: 'carter', id: '1'},
	2: { name: 'Mario',lastName: 'Bross',id: '2' },
	3: { name: 'Luis',lastName: 'Lastra',id: '3' }
};
let usersCount = 3;

server.get('/user', (req, resp, next) => {	
	res.setHeader('Content-Type', 'application/json')
	res.writeHead(200)
	res.end(JSON.stringify(users))	
});

server.get('/user/:id', (req, res, next) => {
	res.setHeader('Content-Type', 'application/json')
	res.writeHead(200);
	res.end(JSON.stringify(users[parseInt(req.params.id)]))
});

server.post('/user', (req, res, next) => {
	console.log(req.body)
	console.log(usersCount)
	let user = req.body
	usersCount++
	user.id = usersCount
	users[user.id] = user
	res.setHeader('Content-type', 'application/json')
	res.writeHead(200)
	res.end(JSON.stringify(user))
});

server.put('/user/:id', (req, res, next) => {
	const user = users[parseInt(req.params.id)]
	const update = req.body
	for(let field in update){
		user[field] = update[field]
	}
	res.setHeader('Content-Type', 'application/json')
	res.writeHead(200)
	res.end(JSON.stringify(user))
});

server.del('/user/:id', (req, res, next) => {
	delete users[parseInt(req.params.id)];
	res.setHeader('Content-Type', 'application/json')
	res.writeHead(200)
	res.end(JSON.stringify(true));
});
*/


