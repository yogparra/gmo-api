
var sqldb       = require("mssql");
var settings    = require("../settings");

module.exports = {
    executesql: (sql, callbalk) => {

        var conn = new sqldb.ConnectionPool(settings.dbconfig)
        conn.connect().then(() => {
            var req = new sqldb.Request(conn)
            req.query(sql).then((recordset) => {
                callbalk(recordset)
                })
                .catch((err) => {
                    console.log(err)
                    callbalk(null, err)
                });
        })
        .catch((err) => {
            console.log(err)
            callbalk(null, err)
        })
    }
}